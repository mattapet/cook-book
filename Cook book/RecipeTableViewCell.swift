//
//  RecipeTableViewCell.swift
//  Cook book
//
//  Created by Peter Matta on 11/7/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit
import SnapKit

class RecipeTableViewCell: UITableViewCell {
    // MARK: - Subview references
    
    weak var recipeImageView: UIImageView!
    weak var durationIcon: UIImageView!
    weak var infoContainerView: UIView!
    weak var name: UILabel!
    weak var duration: UILabel!
    
    // MARK: - Computed properties
    
    var recipe: Recipe? { didSet {
        if let recipe = recipe {
            name?.text = recipe.name
            duration?.text = String(recipe.duration) + "mins"
            setNeedsDisplay()
        }
    }}
    
    
    // MARK: - Initializations
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
        
        let recipeImageView = UIImageView(image: UIImage(named: "img_small"))
        contentView.addSubview(recipeImageView)
        recipeImageView.snp.makeConstraints { (make) in
            make.leading.equalTo(8)
            make.centerY.equalToSuperview()
        }
        recipeImageView.contentMode = .scaleAspectFit
        recipeImageView.setContentHuggingPriority(UILayoutPriority(rawValue: 999), for: .horizontal)
        self.recipeImageView = recipeImageView
        
        let infoContainerView = UIView()
        contentView.addSubview(infoContainerView)
        infoContainerView.snp.makeConstraints { (make) in
            make.leading.equalTo(recipeImageView.snp.trailing).offset(8)
            make.trailing.equalTo(contentView.safeAreaLayoutGuide.snp.trailing).offset(-8)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        self.infoContainerView = infoContainerView
        
        let name = UILabel()
        infoContainerView.addSubview(name)
        name.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        self.name = name
        
        let duration = UILabel()
        infoContainerView.addSubview(duration)
        let durationIcon = UIImageView(image: UIImage(named: "ic_time"))
        infoContainerView.addSubview(durationIcon)
        durationIcon.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.centerY.equalTo(duration)
        }
        durationIcon.contentMode = .scaleAspectFit
        durationIcon.setContentHuggingPriority(UILayoutPriority(rawValue: 999), for: .horizontal)
        self.durationIcon = durationIcon
        
        duration.snp.makeConstraints { (make) in
            make.top.equalTo(name.snp.bottom).offset(8)
            make.leading.equalTo(durationIcon.snp.trailing).offset(8)
        }
        self.duration = duration
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
