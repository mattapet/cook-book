//
//  RecipeTableViewController.swift
//  Cook book
//
//  Created by Peter Matta on 11/7/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit

class RecipeTableViewController: UITableViewController, RecipeManagable {
    // MARK: - Private properties
    
    private let manager = RecipeManager()
    private var recipes = [Recipe]()
    
    // MARK: - View references
    weak var detailVC: DetailViewController?
    
    // MARK: - Initializations
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
        self.title = "Ackee Recipes"
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - View controller lifecycle
    
    override func loadView() {
        super.loadView()
        let addRecipeButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addRecipe))
        navigationItem.setRightBarButton(addRecipeButton, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.beginRefreshing()
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.register(RecipeTableViewCell.self, forCellReuseIdentifier: "RecipeTableViewCell")
        refresh(nil)
    }
    
    
    // MARK: - Data manager communication
    
    @objc func addRecipe() {
        let formVC = RecipeFormViewController()
        let addFormVC = UINavigationController(rootViewController: formVC)
        formVC.delegate = self
        navigationController?.present(addFormVC, animated: true, completion: nil)
    }
    
    @objc func refresh(_ callback: (() -> ())?) {
        manager.getAllRecipes { (recipes) in
            if let recipes = recipes {
                self.recipes = recipes
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeTableViewCell", for: indexPath) as! RecipeTableViewCell
        let recipe = recipes[indexPath.row]
        cell.recipe = recipe
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recipe = recipes[indexPath.row]
        let detailVC = DetailViewController(withRecipe: recipe)
        detailVC.delegate = self
        navigationController?.pushViewController(detailVC, animated: true)
        self.detailVC = detailVC
    }
}


// MARK: - Recipe managable delegate

extension RecipeTableViewController {
    func shouldRefreshRecipes(callback: @escaping () -> ()) {
        refresh(callback)
    }
    
    func shouldGetRecipe(withId id: String, callback: @escaping (Recipe) -> ()) {
        manager.get(withId: id) { (recipe) in
            if let idx = self.recipes.index(where: { $0.id == id }) {
                self.recipes[idx] = recipe
                self.tableView.reloadData()
            }
            callback(recipe)
        }
    }
    
    func shouldCreateRecipe(withValues values: [String : Any], callback: @escaping (Recipe) -> ()) {
        print(values)
        manager.create(withValues: values) { (recipe) in
            self.recipes.append(recipe)
            self.tableView.reloadData()
            callback(recipe)
        }
    }
    
    func shouldUpdateRecipe(withId id: String, withValues values: [String : Any], callback: @escaping (Recipe) -> ()) {
        manager.update(withId: id, withValues: values) { (recipe) in
            if let idx = self.recipes.index(where: { $0.id == id }) {
                self.recipes[idx] = recipe
                self.tableView.reloadData()
                self.detailVC!.recipe = recipe
            }
            callback(recipe)
        }
    }
    
    func shouldRemoveRecipe(withId id: String, callback: @escaping () -> ()) {
        manager.remove(withId: id) {
            if let idx = self.recipes.index(where: { $0.id == id }) {
                self.recipes.remove(at: idx)
                self.tableView.reloadData()
            }
            callback()
        }
    }
    
    func shouldRateRecipe(withId id: String, withValueOf value: Int, callback: @escaping (Recipe) -> ()) {
        manager.rate(withId: id, value: value) { (recipe) in
            if let idx = self.recipes.index(where: { $0.id == id }) {
                self.recipes.remove(at: idx)
                self.tableView.reloadData()
            }
            callback(recipe)
        }
    }
}

