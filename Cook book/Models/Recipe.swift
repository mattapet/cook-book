//
//  Recipe.swift
//  Cook book
//
//  Created by Peter Matta on 11/7/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import Foundation

struct Recipe: Codable {
    var id: String
    var name: String
    var duration: Int
    var score: Double
    var description: String?
    
    var rating: Int?
    var ingredients: [String]?
    var info: String?
}
