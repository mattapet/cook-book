//
//  RecipeViewController.swift
//  Cook book
//
//  Created by Peter Matta on 11/7/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    // MARK: - Private properties
    private var loaded: Bool { get { return recipe.info != nil }}
    
    // MARK: - Properties
    var recipe: Recipe { didSet {
        if let recipeView = recipeView {
            recipeView.removeFromSuperview()
            let recipeView = DetailView(withRecipe: recipe)
            view.addSubview(recipeView)
            recipeView.snp.makeConstraints { (make) in
                make.edges.equalTo(view)
            }
            self.recipeView = recipeView
        }
    }}
    
    // MARK: - Delegate
    
    weak var delegate: RecipeManagable?
    
    
    // MARK: - Subviews references
    
    weak var recipeView: DetailView!
    weak var spinner: UIActivityIndicatorView!
    
    
    // MARK: - Initializations
    
    required init(withRecipe recipe: Recipe) {
        self.recipe = recipe
        super.init(nibName: nil, bundle: nil)
        self.title = recipe.name
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View controller lifecycle
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        view.addSubview(spinner)
        spinner.hidesWhenStopped = true
        spinner.snp.makeConstraints { (make) in
            make.center.equalTo(view.snp.center)
        }
        self.spinner = spinner
        
        let moreOptionsItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(moreInfo))
        navigationItem.setRightBarButton(moreOptionsItem, animated: true)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if loaded {
            showContent()
        } else {
            hideContent()
            delegate?.shouldGetRecipe(withId: recipe.id, callback: { (recipe) in
                self.recipe = recipe
                self.showContent()
                self.view.setNeedsDisplay()
            })
        }
    }
    
    
    // MARK: - Action handling
    
    @objc func moreInfo() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive) { (_) in
            self.hideContent()
            self.delegate?.shouldRemoveRecipe(withId: self.recipe.id) {
                self.navigationController?.popViewController(animated: true)
            }
        })
//        actionSheet.addAction(UIAlertAction(title: "Rate", style: .default, handler: { (_) in
//            print("Rating...")
//        }))
        actionSheet.addAction(UIAlertAction(title: "Edit", style: .default) { (_) in
            let formVC = RecipeFormViewController(withRecipe: self.recipe)
            formVC.delegate = self.delegate
            self.present(UINavigationController(rootViewController: formVC), animated: true, completion: nil)
        })
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    
    // MARK: - Content displaying
    
    private func showContent() {
        spinner.stopAnimating()
        let recipeView = DetailView(withRecipe: recipe)
        view.addSubview(recipeView)
        recipeView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        self.recipeView = recipeView
    }
    
    private func hideContent() {
        recipeView?.isHidden = true
        spinner.startAnimating()
    }
    
}
