//
//  AddFormViewController.swift
//  Cook book
//
//  Created by Peter Matta on 11/13/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit

class RecipeFormViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate {
    // MARK: - Private properties
    private var time: TimeInterval?
    private var recipe: Recipe?
    
    // MARK: - Recipe delegate
    
    weak var delegate: RecipeManagable?
    
    
    // MARK: - View references
    
    weak var spinner: UIActivityIndicatorView!
    weak var disableOverlayView: UIView!
    
    // --- Form components ---
    var basicInputs = [UITextField](arrayLiteral: UITextField(), UITextField())
    var ingredientInputs = [UITextField](arrayLiteral: UITextField())
    var inputs = [UIView]()
    var infoInput = UITextField()
    var descriptionInput = UITextView()
    var nextIngredientLabel: UILabel
    weak var timePicker: UIDatePicker!
    weak var timePickerTextField: UITextField! { get {
        return basicInputs[1]
    }}
    
    
    // MARK: - Initializations
    
    convenience init() {
        self.init(style: .grouped)
        self.title = "Save new recipe"
    }
    
    convenience init(withRecipe recipe: Recipe) {
        self.init()
        self.title = "Update \(recipe.name)"
        self.recipe = recipe
    }
    
    override init(style: UITableViewStyle) {
        nextIngredientLabel = UILabel()
        nextIngredientLabel.text = "Add another ingredience"
        super.init(style: style)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: View controller lifecycle

    override func loadView() {
        super.loadView()
        loadInputs()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultTableViewCell")
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.setLeftBarButton(cancelButton, animated: true)
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save))
        saveButton.isEnabled = false
        navigationItem.setRightBarButton(saveButton, animated: true)
        validate()
    }

    
    // MARK: - Bar button handlers
    
    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func save() {
        disableForm()
        let ingredients = ingredientInputs.filter({ $0.text!.count > 0 }).map { $0.text! }
        let values: Dictionary<String, Any> = [
            "name": basicInputs[0].text!,
            "duration": time!,
            "description": descriptionInput.text!,
            "ingredients": ingredients,
            "info": infoInput.text!
        ]
        
        if let recipe = recipe {
            delegate?.shouldUpdateRecipe(withId: recipe.id, withValues: values) { (recipe) in
                self.cancel()
            }
        } else {
            delegate?.shouldCreateRecipe(withValues: values) { (recipe) in
                self.cancel()
            }
        }
    }
    
    // MARK: - Time selection handlers
    
    @objc func cancelTimeSelection() {
        timePickerTextField.resignFirstResponder()
    }
    
    @objc func confirmTimeSelection() {
        let minutes = Int(timePicker.countDownDuration / 60) % 60
        let hours = Int(timePicker.countDownDuration / 3600)
        time = timePicker.countDownDuration / 60
        timePickerTextField.text = "\(hours)hr \(minutes)min"
        timePickerTextField.resignFirstResponder()
        validate()
    }
    
    // MARK: - Form validation
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        validate()
    }
    
    private func hasIngredience() -> Bool {
        for input in ingredientInputs {
            if input.text!.count > 0 {
                return true
            }
        }
        return false
    }
    
    private func validate() {
        let regex = try! NSRegularExpression(pattern: "ackee", options: .caseInsensitive)
        let name = basicInputs[0].text!
        let matches = regex.matches(in: name, range: NSMakeRange(0, name.count))
        let valid = hasIngredience() && time != nil && matches.count > 0 && descriptionInput.text!.count > 0 && infoInput.text!.count > 0
        navigationItem.rightBarButtonItem!.isEnabled = valid
    }
    
    // MARK: - Rendering from
    
    private func fillDefaults() {
        if let recipe = recipe {
            basicInputs[0].text = recipe.name
            basicInputs[1].text = "\(recipe.duration / 60)hr \(recipe.duration % 60)min"
            time = Double(recipe.duration)
            ingredientInputs = [UITextField]()
            for ingredient in recipe.ingredients! {
                let ingredientInput = UITextField()
                ingredientInput.text = ingredient
                ingredientInputs.append(ingredientInput)
            }
            infoInput.text = recipe.info!
            descriptionInput.text = recipe.description!
        }
    }
    
    private func loadInputs() {
        var inputs = basicInputs
        basicInputs[0].placeholder = "Name"
        basicInputs[1].placeholder = "Duration"

        for input in ingredientInputs {
            input.placeholder = "Ingredient"
            inputs.append(input)
        }
        infoInput.placeholder = "Info"
        inputs.append(infoInput)
        
        for idx in 0..<inputs.count {
            inputs[idx].tag = idx
            setupTextField(inputs[idx])
        }
        descriptionInput.tag = inputs.count
        descriptionInput.returnKeyType = .done
        descriptionInput.delegate = self
        descriptionInput.isEditable = true
        descriptionInput.isScrollEnabled = true
        descriptionInput.font = basicInputs[0].font
        self.inputs = inputs as Array<UIView>
        self.inputs.append(descriptionInput as UIView)
        
        nextIngredientLabel.textColor = nextIngredientLabel.tintColor
        
        let timePicker = UIDatePicker()
        timePickerTextField.inputView = timePicker
        timePickerTextField.tintColor = .clear
        timePicker.datePickerMode = .countDownTimer
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTimeSelection)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(confirmTimeSelection))
        ]
        toolbar.sizeToFit()
        timePickerTextField.inputAccessoryView = toolbar
        self.timePicker = timePicker
        
        fillDefaults()
    }
    
    private func addTextField(_ textField: UITextField, forCell cell: UITableViewCell) {
        cell.contentView.subviews.forEach { (view) in
            view.snp.removeConstraints()
            view.removeFromSuperview()
        }
        cell.contentView.addSubview(textField)
        textField.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-8)
        }
        setupTextField(textField)
    }
    
    private func setupTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.keyboardType = .default
        textField.returnKeyType = .next
        textField.clearButtonMode = .whileEditing
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    private func disableForm() {
        let disabledOverlay = UIView()
        tableView.tableHeaderView = disabledOverlay
        disabledOverlay.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view.superview!)
        }
        disabledOverlay.isOpaque = false
        disabledOverlay.backgroundColor = .white
        disabledOverlay.alpha = 0.5
        self.disableOverlayView = disabledOverlay
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        disabledOverlay.addSubview(spinner)
        spinner.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        spinner.startAnimating()
        self.spinner = spinner
        view.setNeedsLayout()
        view.endEditing(true)
    }
    
    private func enableForm() {
        disableOverlayView?.removeFromSuperview()
        view.setNeedsLayout()
    }
    
    // MARK: - Ingredience handlers
    
    @objc private func addIngredienceInput() {
        let ingredienceInput = UITextField()
        ingredienceInput.placeholder = "Ingredience"
        setupTextField(ingredienceInput)
        ingredientInputs.append(ingredienceInput)
        ingredienceInput.becomeFirstResponder()
        tableView.reloadData()
    }
}


// MARK: - Table view data soruce

extension RecipeFormViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            // Name & duration
        case 0: return 2
            // Ingredience inputs & next ingredience button
        case 1: return ingredientInputs.count + 1
            // Info and description input
        case 2: return 2
            
        default:
            fatalError("There are only three sections in this form.")
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultTableViewCell", for: indexPath) as UITableViewCell
        let idxPath = (section: indexPath.section, row: indexPath.row)
        
        switch idxPath {
        case (0, let row):
            addTextField(basicInputs[row], forCell: cell)
            break
        case (1, let row) where row < ingredientInputs.count:
            addTextField(ingredientInputs[row], forCell: cell)
            break
        case (1, let row) where row == ingredientInputs.count:
            cell.contentView.subviews.forEach({ (view) in
                view.snp.removeConstraints()
                view.removeFromSuperview()
            })
            cell.contentView.addSubview(nextIngredientLabel)
            nextIngredientLabel.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview().offset(16)
                make.trailing.equalToSuperview().offset(-8)
            })
            break
        case (2, 0):
            addTextField(infoInput, forCell: cell)
            break
        case (2, 1):
            cell.contentView.subviews.forEach({ (view) in
                view.snp.removeConstraints()
                view.removeFromSuperview()
            })
            cell.contentView.addSubview(descriptionInput)
            descriptionInput.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview().offset(12)
                make.trailing.equalToSuperview().offset(-8)
            })
            break
        default:break
        }
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 && indexPath.row == 1 {
            return 48 * 3
        } else {
            return 48
        }
    }
}


// MARK: - Table view delegate

extension RecipeFormViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == ingredientInputs.count {
            addIngredienceInput()
        }
    }
}

// MARK: - Text field delegate

extension RecipeFormViewController {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == basicInputs[0] {
            navigationItem.rightBarButtonItem!.isEnabled = false
        }
        validate()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextResponderIdx = textField.tag + 1
        if nextResponderIdx < inputs.count {
            inputs[nextResponderIdx].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}


// MARK: - Text view delegate

extension RecipeFormViewController {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        validate()
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
}



