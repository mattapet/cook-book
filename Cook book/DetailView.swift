//
//  DetailView.swift
//  Cook book
//
//  Created by Peter Matta on 11/7/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import UIKit

class DetailView: UIView {
    // MARK: - Static properties
    
    private static let SECTION_DEVITION_SIZE = 25
    
    
    // MARK: - Private properties
    
    private let recipe: Recipe
    
    // MARK: - Label references
    
    weak var info: UILabel!
    weak var scrollView: UIScrollView!
    weak var duration: UILabel!
    weak var ingredienceHeader: UILabel!
    var ingredientList: [UILabel] = [UILabel]()
    weak var descriptionHeader: UILabel!
    weak var recipeDescription: UILabel!
    weak var contentView: UIView!
    
    
    // MARK: - Initializations
    
    required init(withRecipe recipe: Recipe) {
        self.recipe = recipe
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        // ---- Scroll view ----
        
        let scrollView = UIScrollView()
        addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        scrollView.bounces = true
        scrollView.showsVerticalScrollIndicator = true
        scrollView.alwaysBounceVertical = true
        self.scrollView = scrollView
        
        // ---- Content view ----
        
        let contentView = UIView()
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(scrollView)
        }
        self.contentView = contentView
        
        
        // ---- Detail content views ----
        
        let info = UILabel()
        info.text = recipe.info!
        contentView.addSubview(info)
        info.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.top).offset(10)
            make.leading.equalTo(self.safeAreaLayoutGuide.snp.leading).offset(10)
            make.trailing.equalTo(self.safeAreaLayoutGuide.snp.trailing).offset(-10)
            
        }
        info.numberOfLines = 0
        self.info = info
        
        
        let duration = UILabel()
        duration.text = "Duration: \(recipe.duration / 60) hours \(recipe.duration % 60) minutes"
        contentView.addSubview(duration)
        duration.snp.makeConstraints { (make) in
            make.top.equalTo(info.snp.bottom).offset(8)
            make.leading.equalTo(info)
            make.trailing.equalTo(info)
        }
        
        
        let ingredienceHeader = UILabel()
        contentView.addSubview(ingredienceHeader)
        ingredienceHeader.snp.makeConstraints { (make) in
            make.top.equalTo(duration.snp.bottom).offset(DetailView.SECTION_DEVITION_SIZE)
            make.leading.equalTo(info)
            make.trailing.equalTo(info)
        }
        ingredienceHeader.text = "Ingredients:"
        self.ingredienceHeader = ingredienceHeader
        ingredienceHeader.font = UIFont.systemFont(ofSize: CGFloat(24))
        
        
        let ingredienceList = UILabel()
        contentView.addSubview(ingredienceList)
        for ingredient in recipe.ingredients! {
            let label = UILabel()
            label.text = "• \(ingredient)\n"
            contentView.addSubview(label)
            label.snp.makeConstraints({ (make) in
                if let previous = ingredientList.last {
                    make.top.equalTo(previous.snp.bottom).offset(5)
                } else {
                    make.top.equalTo(ingredienceHeader.snp.bottom).offset(5)
                }
                make.leading.equalTo(ingredienceHeader)
                make.trailing.equalTo(ingredienceHeader)
            })
            ingredientList.append(label)
        }
        
        
        let descriptionHeader = UILabel()
        contentView.addSubview(descriptionHeader)
        descriptionHeader.snp.makeConstraints { (make) in
            make.top.equalTo(ingredientList.last!.snp.bottom).offset(DetailView.SECTION_DEVITION_SIZE)
            make.leading.equalTo(ingredientList.last!)
            make.trailing.equalTo(ingredientList.last!)
        }
        descriptionHeader.text = "Description:"
        descriptionHeader.font = UIFont.systemFont(ofSize: CGFloat(24))
        self.descriptionHeader = descriptionHeader
        
        
        let descrioption = UILabel()
        contentView.addSubview(descrioption)
        descrioption.snp.makeConstraints { (make) in
            make.top.equalTo(descriptionHeader.snp.bottom).offset(5)
            make.leading.equalTo(descriptionHeader)
            make.trailing.equalTo(descriptionHeader)
            make.bottom.equalTo(contentView).offset(-10)
        }
        descrioption.text = recipe.description
        descrioption.numberOfLines = 0
        self.recipeDescription = descrioption
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
