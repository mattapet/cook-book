//
//  RecipeManager.swift
//  Cook book
//
//  Created by Peter Matta on 11/7/17.
//  Copyright © 2017 Peter Matta. All rights reserved.
//

import Foundation
import Alamofire

protocol RecipeManagable: NSObjectProtocol {
    func shouldRefreshRecipes(callback: @escaping () -> ())
    func shouldGetRecipe(withId: String, callback: @escaping (Recipe) -> ())
    func shouldCreateRecipe(withValues: [String:Any], callback: @escaping (Recipe) -> ())
    func shouldUpdateRecipe(withId: String, withValues: [String:Any], callback: @escaping (Recipe) -> ())
    func shouldRemoveRecipe(withId: String, callback: @escaping () -> ())
    func shouldRateRecipe(withId: String, withValueOf: Int, callback: @escaping (Recipe) -> ())
}

class RecipeManager {
    let BASE_URL = "https://cookbook.ack.ee/api/v1/recipes"
    
    func getAllRecipes(callback: @escaping ([Recipe]?) -> ()) {
        Alamofire.request(BASE_URL).responseJSON { (response) in
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let recipes = try decoder.decode([Recipe].self, from: data)
                    callback(recipes)
                } catch { callback(nil) }
            }
        }
    }
    
    func get(withId id: String, callback: @escaping (Recipe) -> ()) {
        Alamofire.request("\(BASE_URL)/\(id)").responseJSON { (response) in
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let recipe = try decoder.decode(Recipe.self, from: data)
                    callback(recipe)
                } catch {}
            }
        }
    }
    
    func create(withValues values: [String:Any], callback: @escaping (Recipe) -> ()) {
        Alamofire.request("\(BASE_URL)", method: .post, parameters: values, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                print(response)
                if let data = response.data {
                    do {
                        let decoder = JSONDecoder()
                        let recipe = try decoder.decode(Recipe.self, from: data)
                        callback(recipe)
                    } catch {}
                }
        }
    }
    
    func update(withId id: String, withValues values: [String:Any], callback: @escaping (Recipe) -> ()) {
        Alamofire.request("\(BASE_URL)/\(id)", method: .put, parameters: values, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                if let data = response.data {
                    do {
                        let decoder = JSONDecoder()
                        let recipe = try decoder.decode(Recipe.self, from: data)
                        callback(recipe)
                    } catch {}
                }
        }
    }
    
    func remove(withId id: String, callback: @escaping () -> ()) {
        Alamofire.request("\(BASE_URL)/\(id)", method: .delete)
            .responseJSON { (response) in
                callback()
        }
    }
    
    func rate(withId id: String, value: Int, callback: @escaping (Recipe) -> ()) {
        Alamofire.request("\(BASE_URL)/\(id)/ratings", method: .post, parameters: ["rating":value], encoding: JSONEncoding.default)
            .responseJSON { (response) in
                if let data = response.data {
                    do {
                        let decoder = JSONDecoder()
                        let recipe = try decoder.decode(Recipe.self, from: data)
                        callback(recipe)
                    } catch {}
                }
            }
    }
}
